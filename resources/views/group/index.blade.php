@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Quản lý group</h1>
    <div>
        <a href="{{route('group.create')}}" class="btn btn-primary">Tạo group</a>
    </div>
    <div>
        @include('message')

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">#</th>
                    <th>Tên</th>
                    <th width="7%">DS User</th>
                    <th width="7%">Phân quyền</th>
                    <th width="5%">Xem</th>
                    <th width="5%">Sửa</th>
                    <th width="5%">Xóa</th>
                </tr>
                </thead>
                <tbody>
                @foreach($groups as $group)
                    <tr>
                        <td>{{$group->id}}</td>
                        <td>{{$group->name}}</td>
                        <td>
                            <a class="btn btn-primary" href="{{route('group.listUser',['id'=>$group->id])}}">DS User</a>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{route('group.listPermission',['id'=>$group->id])}}">Phân quyền</a>
                        </td>
                        <td>
                            <a class="btn btn-primary" href="{{route('group.show',['id'=>$group->id])}}">Xem</a>
                        </td>
                        <td>
                            <a class="btn btn-info" href="{{route('group.edit',['id'=>$group->id])}}">Sửa</a>
                        </td>
                        <td>
                            <form action="{{route('group.delete',['id'=>$group->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
