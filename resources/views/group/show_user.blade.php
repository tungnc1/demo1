@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Danh sách nhóm user</h1>
    <div class="m-auto w-50">
        <a class="btn btn-primary" href="{{route('group.index')}}">Danh sách</a>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>User</th>
                        @foreach($groups as $group)
                            <th>{{$group->name}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->email}}</td>
                            @foreach($groups as $group)
                                <td>
                                    @foreach($user->groups as $user_group)
                                        @if($user_group->id == $group->id) 1 @endif
                                    @endforeach
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
