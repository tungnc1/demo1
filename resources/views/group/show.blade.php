@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Xem chi tiết group #{{$group->id}}</h1>
    <div class="m-auto w-50">
        <a class="btn btn-primary" href="{{route('group.index')}}">Danh sách</a>
        <p class="mt-3">ID: {{$group->id}}</p>
        <p>Tên: {{$group->name}}</p>
    </div>
@endsection
