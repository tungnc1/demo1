@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Phân quyền {{$group->name}}</h1>
    <div>
        @include('message')

        <div>
            <form action="{{route('permission.store',['group_id'=>$group->id])}}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="object">Object:</label>
                            <input id="object" class="form-control @error('object') is-invalid @enderror" name="object" type="text" required autocomplete="off">
                            @error('object')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="mb-3">
                            <label for="action">Action:</label>
                            <input id="action" class="form-control @error('action') is-invalid @enderror" name="action" type="text" required autocomplete="off">
                            @error('action')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </form>
        </div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">#</th>
                    <th>Object</th>
                    <th>Action</th>
                    <th width="5%">Xóa</th>
                </tr>
                </thead>
                <tbody>
                @foreach($permissions as $permission)
                    <tr>
                        <td>{{$permission->id}}</td>
                        <td>{{$permission->object}}</td>
                        <td>{{$permission->action}}</td>
                        <td>
                            <form action="{{route('permission.delete',['id'=>$permission->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
