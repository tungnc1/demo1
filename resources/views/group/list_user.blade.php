@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Danh sách nhóm {{$group->name}}</h1>
    <div class="m-auto w-50">
        <a class="btn btn-primary" href="{{route('group.index')}}">Danh sách</a>
        <div class="table-responsive">
            <form action="{{route('group.saveGroupUser',['id'=>$group->id])}}" method="POST">
                @csrf
                <table class="table">
                    <thead>
                    <tr>
                        <th>User</th>
                        <th>Active</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->email}}</td>
                            <td>
                                <input class="form-check-input" type="checkbox" name="group_user[{{$user->id}}]"
                                       @foreach($user->groups as $user_group)
                                            @if($user_group->id == $group->id) checked @endif
                                       @endforeach
                                >
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">Lưu</button>
            </form>
        </div>
    </div>
@endsection
