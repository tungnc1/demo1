@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Tạo group</h1>
    <div class="m-auto w-50">
        <form action="{{route('group.store')}}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="name">Tên:</label>
                <input id="name" class="form-control @error('name') is-invalid @enderror" name="name" type="text" required autocomplete="off">
                @error('name')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <a class="btn btn-dark" href="{{route('group.index')}}">Hủy</a>
            <button type="submit" class="btn btn-primary">Lưu</button>
        </form>
    </div>
@endsection
