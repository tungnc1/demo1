@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Xem chi tiết user #{{$user->id}}</h1>
    <div class="m-auto w-50">
        <a class="btn btn-primary" href="{{route('user.index')}}">Danh sách</a>
        <p class="mt-3">ID: {{$user->id}}</p>
        <p>Tên: {{$user->name}}</p>
        <p>Email: {{$user->email}}</p>
        <p>IsAdmin: {{$user->is_admin}}</p>
    </div>
@endsection
