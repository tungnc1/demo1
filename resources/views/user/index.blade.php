@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Quản lý user</h1>
    <div>
        @include('message')

        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th width="5%">#</th>
                    <th>Tên</th>
                    <th width="25%">Email</th>
                    <th width="5%">IsAdmin</th>
                    <th width="5%">Xem</th>
                    <th width="5%">Sửa</th>
                    <th width="5%">Xóa</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->is_admin}}</td>
                        <td>
                            <a class="btn btn-primary" href="{{route('user.show',['id'=>$user->id])}}">Xem</a>
                        </td>
                        <td>
                            <a class="btn btn-info" href="{{route('user.edit',['id'=>$user->id])}}">Sửa</a>
                        </td>
                        <td>
                            <form action="{{route('user.delete',['id'=>$user->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
