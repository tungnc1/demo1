@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Sửa user #{{$user->id}}</h1>
    <div class="m-auto w-50">
        <form action="{{route('user.update',['id'=>$user->id])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="name">Tên:</label>
                <input id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$user->name}}" type="text" required autocomplete="off">
                @error('name')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="email">Email:</label>
                <input id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email}}" type="email" required autocomplete="off">
                @error('email')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="mb-3 form-check form-switch">
                <input class="form-check-input" type="checkbox" name="is_admin" id="flexSwitchCheckChecked" @if($user->is_admin == 1) checked @endif>
                <label class="form-check-label" for="flexSwitchCheckChecked">IsAdmin</label>
            </div>
            <a class="btn btn-dark" href="{{route('user.index')}}">Hủy</a>
            <button type="submit" class="btn btn-primary">Lưu</button>
        </form>
    </div>
@endsection
