@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Quản lý location</h1>
    <div>
        @include('message')

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th width="5%">#</th>
                        <th>Tên</th>
                        <th width="5%">Xem</th>
                        <th width="5%">Sửa</th>
                        <th width="5%">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($locations as $location)
                    <tr>
                        <td>{{$location->id}}</td>
                        <td>{{$location->name}}</td>
                        <td>
                            <a class="btn btn-primary" href="{{route('location.show',['id'=>$location->id])}}">Xem</a>
                        </td>
                        <td>
                            <a class="btn btn-info" href="{{route('location.edit',['id'=>$location->id])}}">Sửa</a>
                        </td>
                        <td>
                            <form action="{{route('location.delete',['id'=>$location->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Xóa</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
