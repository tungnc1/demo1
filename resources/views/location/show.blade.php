@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Xem chi tiết location #{{$location->id}}</h1>
    <div class="m-auto w-50">
        <a class="btn btn-primary" href="{{route('location.index')}}">Danh sách</a>
        <p class="mt-3">ID: {{$location->id}}</p>
        <p>Tên: {{$location->name}}</p>
    </div>
@endsection
