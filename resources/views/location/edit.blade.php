@extends('layout')

@section('content')
    <h1 class="mt-8 text-center">Sửa location #{{$location->id}}</h1>
    <div class="m-auto w-50">
        <form action="{{route('location.update',['id'=>$location->id])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="name">Tên:</label>
                <input id="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$location->name}}" type="text" required autocomplete="off">
                @error('name')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <a class="btn btn-dark" href="{{route('location.index')}}">Hủy</a>
            <button type="submit" class="btn btn-primary">Lưu</button>
        </form>
    </div>
@endsection
