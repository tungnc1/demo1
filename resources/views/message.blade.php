@if(session()->has('success_msg'))
    <div class="mb-3">
        <div class="alert alert-success">
            {{ session()->get('success_msg') }}
        </div>
    </div>
@endif
@if(session()->has('error_msg'))
    <div class="mb-3">
        <div class="alert alert-danger">
            {{ session()->get('error_msg') }}
        </div>
    </div>
@endif
