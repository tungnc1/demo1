<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\PermissionController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', [LoginController::class,'index'])->name('login');
Route::post('/login', [LoginController::class,'login'])->name('doLogin');
Route::get('/logout',[LogoutController::class,'logout'])->name('logout');

Route::prefix('location')->group(function (){
    Route::get('/',[LocationController::class,'index'])->name('location.index')->middleware('auth');
    Route::get('/show/{id}',[LocationController::class,'show'])->name('location.show')->middleware('auth');
    Route::get('/edit/{id}',[LocationController::class,'edit'])->name('location.edit')->middleware('auth');
    Route::put('/edit/{id}',[LocationController::class,'update'])->name('location.update')->middleware('auth');
    Route::delete('/delete/{id}',[LocationController::class,'destroy'])->name('location.delete')->middleware('auth');
});

Route::prefix('user')->group(function (){
    Route::get('/',[UserController::class,'index'])->name('user.index')->middleware('auth');
    Route::get('/show/{id}',[UserController::class,'show'])->name('user.show')->middleware('auth');
    Route::get('/edit/{id}',[UserController::class,'edit'])->name('user.edit')->middleware('auth');
    Route::put('/edit/{id}',[UserController::class,'update'])->name('user.update')->middleware('auth');
    Route::delete('/delete/{id}',[UserController::class,'destroy'])->name('user.delete')->middleware('auth');
});

Route::prefix('group')->group(function (){
    Route::get('/',[GroupController::class,'index'])->name('group.index')->middleware('auth');
    Route::get('/create',[GroupController::class,'create'])->name('group.create')->middleware('auth');
    Route::post('/create',[GroupController::class,'store'])->name('group.store')->middleware('auth');
    Route::get('/show/{id}',[GroupController::class,'show'])->name('group.show')->middleware('auth');
    Route::get('/edit/{id}',[GroupController::class,'edit'])->name('group.edit')->middleware('auth');
    Route::put('/edit/{id}',[GroupController::class,'update'])->name('group.update')->middleware('auth');
    Route::delete('/delete/{id}',[GroupController::class,'destroy'])->name('group.delete')->middleware('auth');
    Route::get('/list-user/{id}',[GroupController::class,'listUser'])->name('group.listUser')->middleware('auth');
    Route::post('/list-user/{id}',[GroupController::class,'saveGroupUser'])->name('group.saveGroupUser')->middleware('auth');
    Route::get('/list-permission/{id}',[GroupController::class,'listPermission'])->name('group.listPermission')->middleware('auth');
});

Route::prefix('permission')->group(function (){
    Route::post('/create/{group_id}',[PermissionController::class,'store'])->name('permission.store')->middleware('auth');
    Route::delete('/delete/{id}',[PermissionController::class,'destroy'])->name('permission.delete')->middleware('auth');
});
