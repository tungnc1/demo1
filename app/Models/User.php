<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Arr;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function groups(){
        return $this->belongsToMany(Group::class,'user_groups','user_id','group_id');
    }

    public function checkPermission($object, $action){
        if (auth()->user()->is_admin == 1){
            return true;
        }
        $groups = auth()->user()->groups;
        $arr = $groups->toArray();
        $slice = Arr::pluck($arr, 'id');

        $permission = Permission::query()
            ->where('object','like',$object)
            ->where('action','like',$action)
            ->whereIn('group_id',$slice)->count();
        if ($permission > 0){
            return true;
        }

        return false;

    }
}
