<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    use HasFactory;

    protected $table = 'user_groups';
    protected $fillable = [
        'user_id',
        'group_id'
    ];
//    $permissions = Permission::query()
//    ->where('object','like',$object)
//    ->where('action','like',$action)
//    ->get();
//
//    $arrUserGroups = UserGroup::query()->where('user_id','=',$this->id)->pluck('group_id');
//
//    foreach ($permissions as $permission){
//    if (in_array($permission->group_id,$arrUserGroups->all())){
//    return true;
//    }
//    }
}
