<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->cannot('viewAny', User::class)) {
            abort(403);
        }
        $users = User::query()->get();
        return view('user.index',[
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->user()->cannot('view', User::class)) {
            abort(403);
        }
        $user = User::findOrFail($id);
        return view('user.show',[
            'user' => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->cannot('update', User::class)) {
            abort(403);
        }
        $user = User::findOrFail($id);
        return view('user.edit',[
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->cannot('update', User::class)) {
            abort(403);
        }
        $data = $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email']
        ]);
        if ($request->has('is_admin')){
            $data['is_admin'] = 1;
        }

        $user = User::find($id);

        if (is_null($user)){
            return redirect()->route('user.index')->with('error_msg','Sửa thất bại!');
        }
        try {
            $user->update($data);
            return redirect()->route('user.index')->with('success_msg','Sửa thành công.');
        }catch (\Exception $e){
            return redirect()->route('user.index')->with('error_msg',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->cannot('delete', User::class)) {
            abort(403);
        }
        $user = User::find($id);
        if (is_null($user)){
            return redirect()->route('user.index')->with('error_msg','Xóa thất bại!');
        }
        UserGroup::query()->where('user_id','=',$user->id)->delete();
        $user->delete();
        return redirect()->route('user.index')->with('success_msg','Xóa thành công.');
    }
}
