<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $group_id)
    {
        $group = Group::find($group_id);
        if (is_null($group)){
            return back()->with('error_msg','Đã có lỗi xảy ra.');
        }
        $data = $request->validate([
            'object' => ['required', 'string'],
            'action' => ['required', 'string']
        ]);
        $data['group_id'] = $group_id;
        try {
            Permission::create($data);
            return redirect()->route('group.listPermission',['id'=>$group_id])->with('success_msg','Tạo thành công.');
        }catch (\Exception $e){
            return redirect()->route('group.listPermission',['id'=>$group_id])->with('error_msg',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::find($id);
        if(is_null($permission)){
            return back()->with('error_msg','Đã có lỗi xảy ra.');
        }
        $group_id = $permission->group_id;
        $permission->delete();
        return redirect()->route('group.listPermission',['id'=>$group_id])->with('success_msg','Xóa thành công.');
    }
}
