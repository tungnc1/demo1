<?php

namespace App\Http\Controllers;

use App\Models\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->cannot('viewAny', Location::class)) {
            abort(403);
        }
        $locations = Location::query()->get();
        return view('location.index',[
            'locations' => $locations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->user()->cannot('view', Location::class)) {
            abort(403);
        }
        $location = Location::findOrFail($id);
        return view('location.show',[
            'location' => $location
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->cannot('update', Location::class)) {
            abort(403);
        }
        $location = Location::findOrFail($id);
        return view('location.edit',[
            'location' => $location
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->cannot('update', Location::class)) {
            abort(403);
        }
        $data = $request->validate([
            'name' => ['required', 'string']
        ]);
        $location = Location::find($id);

        if (is_null($location)){
            return redirect()->route('location.index')->with('error_msg','Sửa thất bại!');
        }
        try {
            $location->update($data);
            return redirect()->route('location.index')->with('success_msg','Sửa thành công.');
        }catch (\Exception $e){
            return redirect()->route('location.index')->with('error_msg',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->cannot('delete', Location::class)) {
            abort(403);
        }
        $location = Location::find($id);
        if (is_null($location)){
            return redirect()->route('location.index')->with('error_msg','Xóa thất bại!');
        }
        $location->delete();
        return redirect()->route('location.index')->with('success_msg','Xóa thành công.');
    }
}
