<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Permission;
use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->cannot('viewAny', Group::class)) {
            abort(403);
        }
        $groups = Group::query()->get();
        return view('group.index',[
            'groups' => $groups
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->user()->cannot('create', Group::class)) {
            abort(403);
        }
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->cannot('create', Group::class)) {
            abort(403);
        }
        $data = $request->validate([
            'name' => ['required', 'string']
        ]);

        try {
            Group::create($data);
            return redirect()->route('group.index')->with('success_msg','Tạo thành công.');
        }catch (\Exception $e){
            return redirect()->route('group.index')->with('error_msg',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->user()->cannot('view', Group::class)) {
            abort(403);
        }
        $group = Group::findOrFail($id);
        return view('group.show',[
            'group' => $group
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if ($request->user()->cannot('update', Group::class)) {
            abort(403);
        }
        $group = Group::findOrFail($id);
        return view('group.edit',[
            'group' => $group
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->user()->cannot('update', Group::class)) {
            abort(403);
        }
        $data = $request->validate([
            'name' => ['required', 'string']
        ]);
        $group = Group::find($id);

        if (is_null($group)){
            return redirect()->route('group.index')->with('error_msg','Sửa thất bại!');
        }
        try {
            $group->update($data);
            return redirect()->route('group.index')->with('success_msg','Sửa thành công.');
        }catch (\Exception $e){
            return redirect()->route('group.index')->with('error_msg',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->cannot('delete', Group::class)) {
            abort(403);
        }
        $group = Group::find($id);
        if (is_null($group)){
            return redirect()->route('group.index')->with('error_msg','Xóa thất bại!');
        }
        UserGroup::query()->where('group_id','=',$group->id)->delete();
        $group->delete();
        return redirect()->route('group.index')->with('success_msg','Xóa thành công.');
    }

    public function listUser(Request $request, $id){
        if ($request->user()->cannot('showListUser', Group::class)) {
            abort(403);
        }
        $group = Group::findOrFail($id);
        $users = User::with('groups:id,name')->get();
        return view('group.list_user',[
            'users' => $users,
            'group' => $group
        ]);
    }

    public function saveGroupUser(Request $request, $id){
        if ($request->user()->cannot('saveGroupUser', Group::class)) {
            abort(403);
        }
        $group = Group::find($id);
        if (is_null($group)){
            return redirect()->route('group.index')->with('error_msg','Sửa thất bại!');
        }
        try {
            UserGroup::query()->where('group_id','=',$id)->delete();
            if ($request->has('group_user')){
                foreach($request->group_user as $index => $item){
                    UserGroup::create(['user_id'=>$index,'group_id'=>$id]);
                }
            }
            return redirect()->route('group.index')->with('success_msg','Sửa thành công.');
        }catch (\Exception $e){
            return redirect()->route('group.index')->with('error_msg',$e->getMessage());
        }
    }

    public function listPermission($id){
        $group = Group::findOrFail($id);
        $permissions = Permission::query()->where('group_id','=',$id)->get();
        return view('group.show_permission',[
            'group' => $group,
            'permissions' => $permissions
        ]);
    }
}
